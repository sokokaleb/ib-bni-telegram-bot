const casper = require('casper').create({
    pageSettings: {
        loadPlugins: true,
        localToRemoteUrlAccessEnabled: true,
        userAgent: 'Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'
    },
    verbose: false,
    logLevel: 'debug',
    exitOnError: 'false'
});

const opts = casper.cli.options;

const TARGET_URL = opts.URL;
const IBB_USERNAME = opts.IUSR;
const IBB_PASSWORD = opts.IPAS;

const BASE_CAPT_PATH = '/vagrant/';
var capt_counter = 0;

function capture(obj) {
    ++capt_counter;
    obj.capture(BASE_CAPT_PATH + 'Capture' + capt_counter + '.png');
}

casper.start(TARGET_URL, function() {
});

casper.waitForText('Pilih Bahasa/Choose Language:', function() {
    capture(this);
    this.clickLabel('MASUK');
});

casper.waitForText('User Id', function() {
    capture(this);

    this.fillSelectors('form#form', {
        'input[name="CorpId"]'   : IBB_USERNAME,
        'input[name="PassWord"]' : IBB_PASSWORD
    }, false);

    this.click('input[type="submit"]');
});

casper.waitForText('PUSAKA SETYABUDI', function() {
    capture(this);
    this.clickLabel('REKENING');
});

casper.waitForText('MUTASI REKENING', function() {
    capture(this);
    this.clickLabel('MUTASI REKENING');
});

casper.waitForText('Pilih Rekening', function() {
    capture(this);

    this.evaluate(function() {
        var dd = document.querySelector('select[name="MAIN_ACCOUNT_TYPE"]');
        dd.value = 'OPR';
        dd.onChange();
    });

    this.click('input[type="submit"][name="AccountIDSelectRq"]');
});

casper.waitForText('Kriteria Pencarian', function() {
    capture(this);
    this.evaluate(function() {
        var dd = document.querySelector('select[name="TxnPeriod"]');
        dd.value = 'LastMonth';
        dd.onChange();
    });
    capture(this);

    this.click('input[value="Lanjut"]');
});

var data = [];

casper.waitForText('Nomor Rekening', function() {
    capture(this);

    var nPages = this.evaluate(function (){
        var nPages = document.querySelectorAll('#PageNumberData').length;
        nPages = Math.max(nPages, 1);
        return nPages;
    });
    nPages = nPages || 1;

    var takeData = function(currentPage, nPages) {
        capture(casper);
        var res = casper.evaluate(function() {
            var res = [];
            var rows = document.querySelectorAll('span#H.BodytextCol2, span#H.DrText, span#H.CrText');
            for (var i = 2; i < rows.length; i += 6) {
                var date = rows[i].innerText;
                var desc = rows[i + 1].innerText;
                var type = rows[i + 2].innerText;
                var amt  = rows[i + 3].innerText;
                var bal  = rows[i + 4].innerText;

                res.push({
                    date: date,
                    desc: desc,
                    type: type,
                    amt: amt,
                    bal: bal
                });
            }
            return res;
        });
        data = data.concat(res);

        if (currentPage < nPages) {
            casper.thenClick('a#NextData');
            casper.waitFor(function() {
                var page = casper.getElementInfo('span#PageNumberData').text;
                page = page.trim();
                return page == currentPage + 1;
            }, function() {
                takeData(currentPage + 1, nPages);
            });
        }
        return ;
    };

    this.then(function() {
        takeData(1, nPages);
    });
});

casper.then(function() {
    // BNI stored the rows sorted by time from the newest to oldest
    data = data.reverse();
    console.log(JSON.stringify(data));

    this.click('input[type="submit"][name="LogOut"]');
});

casper.waitForText('Apakah Anda yakin untuk keluar', function() {
    capture(this);
    this.click('input[type="submit"][name="__LOGOUT__"]');
});

casper.waitForText('Anda telah berhasil keluar', function() {
    capture(this);
});

casper.run();
